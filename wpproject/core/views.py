# conding=utf-8

from django.shortcuts import render
from django.http import HttpResponse

from django.views.generic import View, TemplateView, RedirectView

# def index(request):
# 	return render(request, 'index.html')

class IndexView(RedirectView):
	template_name = 'index.html'

# index = IndexView.as_view()

index = RedirectView.as_view(permanent=False, url='catalogo')