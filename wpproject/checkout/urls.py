# coding=utf-8

from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'checkout'
urlpatterns = [
    url(
        r'^carrinho/adicionar/(?P<slug>[\w_-]+)/$', views.create_cartitem,
        name='create_cartitem'
    ),
	path('carrinho/', views.cart_item, name="cart_item"),
	path('finalizando/', views.checkout, name="checkout"),
	path('meus-pedidos/', views.order_list, name="order_list"),

]
