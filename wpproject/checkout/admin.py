from django.contrib import admin

# Register your models here.

# from .models import *
from .models import CartItem, Order, OrderItem


class CartItemAdmin(admin.ModelAdmin):
	list_display = ['product', 'quantity', 'price']
	seach_fields = ['product', 'quantity', 'price']
	list_filter = ['product', 'quantity', 'price']


class OrderAdmin(admin.ModelAdmin):
	list_display = ['user', 'status', 'payment_option', Order.total]
	seach_fields = ['user', 'status', 'created', 'modified']
	list_filter = ['user', 'status', 'created', 'modified']
	
# admin.site.register(CartItem, CartItemAdmin)
# admin.site.register([Order, OrderItem])
admin.site.register(Order, OrderAdmin)