# coding=utf-8

from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'catalog'
urlpatterns = [
	path('', views.product_list, name="product_list"),
	# path('(?P<slug>[\w_-]+)/$', views.category, name="category"),
    url(r'^(?P<slug>[\w_-]+)/$', views.category, name='category'),
    url(r'^produtos/(?P<slug>[\w_-]+)/$', views.product, name='product'),
]
