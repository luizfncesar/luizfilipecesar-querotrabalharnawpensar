from django.contrib import admin

# Register your models here.

from .models import *
from checkout.models import CartItem, Order, OrderItem

class CategoryAdmin(admin.ModelAdmin):
	list_display = ['name', 'slug', 'created', 'modified']
	seach_fields = ['name', 'slug']
	list_filter = ['created', 'modified']

class ProductAdmin(admin.ModelAdmin):
	list_display = ['name', 'slug', 'created', 'modified', 'price', ]
	seach_fields = ['name', 'slug', 'category__name']
	list_filter = ['created', 'modified']
	

admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)