# coding=utf-8

from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'accounts'
urlpatterns = [
    path('', views.index, name='index'),
    path('registro/', views.register, name='register'),
    path('alterar-dados/', views.update_user, name='update_user'),
]
