from django.shortcuts import render
from django.http import HttpResponse

from django.urls import reverse_lazy
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import View, TemplateView, CreateView, UpdateView
from django.contrib.auth import get_user_model

from django.contrib.auth.mixins import LoginRequiredMixin


class IndexView(LoginRequiredMixin, TemplateView):
	template_name = 'accounts/index.html'



User = get_user_model()

class RegisterView(CreateView):
	form_class = UserCreationForm
	template_name = 'register.html'
	model = User
	success_url = reverse_lazy('index')


class UpdateUserView(UpdateView):
	template_name = 'accounts/update_user.html'
	model = User
	fields = ['username', 'email']
	success_url = reverse_lazy('accounts:index')

	def get_object(self):
		return self.request.user


index = IndexView.as_view()
register = RegisterView.as_view()
update_user = UpdateUserView.as_view()


